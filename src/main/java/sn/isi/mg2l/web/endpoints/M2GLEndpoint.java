package sn.isi.mg2l.web.endpoints;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import sn.isi.mg2l.service.UserService;
import sn.isi.mg2l.wsdl.GetInfoRequest;
import sn.isi.mg2l.wsdl.GetInfoResponse;

@Endpoint
public class M2GLEndpoint {

    @Autowired
    UserService userService;


    @PayloadRoot(namespace = "http://ws.groupeisi.com",localPart = "getInfoRequest")
    public @ResponsePayload GetInfoResponse getInfoRequest (@RequestPayload GetInfoRequest request) {
        GetInfoResponse response= new GetInfoResponse();
        response.setOutput("Bonjour M2GL "+ request.getInput());
        return response;
    }


}
