import { NgModule } from '@angular/core';

import { Monomg2LSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [Monomg2LSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [Monomg2LSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class Monomg2LSharedCommonModule {}
