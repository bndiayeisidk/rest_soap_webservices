package sn.isi.mg2l.web.rest;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class M2GL {

    @GetMapping("/classe")
    public ResponseEntity<String> getNomdeLaClasse() {


        return new ResponseEntity<>("{" + "\"name\":\"M2GL\"," +
            "\"effectif\": 24, "+
            "\"Niveau\": \"Master2\""+
            "}", HttpStatus.OK);
    }
}
