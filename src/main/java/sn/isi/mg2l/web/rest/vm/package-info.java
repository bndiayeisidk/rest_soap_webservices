/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.isi.mg2l.web.rest.vm;
